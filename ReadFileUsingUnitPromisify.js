const { promisify } = require("util");
const fs = require("fs");

let mineReadFile = promisify(fs.readFile);

mineReadFile('./resources/File.txt').then(value=>{
  console.log(value.toString());
});
