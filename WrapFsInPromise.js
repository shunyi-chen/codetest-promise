
/**
 * 封装一个函数
 * 参数：path
 * 返回：Promise对象
 */
function mineReadFile(path) {
  return new Promise((resolve, reject) => {
    require('fs').readFile(path, (err, data) => {
      if(err) reject(err);
      resolve(data);
    });
  });
}

mineReadFile('./resources/File.txt')
.then(value => {
  console.log(value.toString());
}, reason=> {
  console.warn(reason);
});


