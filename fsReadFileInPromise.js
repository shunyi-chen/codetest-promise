const fs = require('fs');

//回调函数形势
// fs.readFile('./resources/File.txt', (err, data) => {
//   //如果出错，抛出错误
//   if(err) throw err;
//   //否则输出内容
//   console.log(data.toString());
// });

//Promise 调用方式
const p = new Promise((resolve2, reject) => {
  fs.readFile('./resources/File.txt', (err, data) => {
    //如果出错，抛出错误
    if(err) reject(err);
    //否则输出内容
    resolve2(data);
  });
});

p.then(value => {
  console.log(value.toString());
}, reason => {
  console.log(reason);
});