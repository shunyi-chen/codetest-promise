let p = new Promise((resolve, reject) => {
  resolve('OK');
});

p.then(value => {
  return new Promise((resolve, reject) => {
    resolve(value);
  });
}).then(value => {
  console.log("value1="+value);

    //中断链式调用，有且只有一种方法
    return new Promise(()=>{});

}).then(value => {
  console.log("value2="+value);
});

