function Promise(executor) {

    //添加属性
    this.PromiseState = 'pending';
    this.PromiseResult = null;
    this.callback = [];

    const self = this;

    function resolve(data) {
        if (self.PromiseState !== 'pending') return;
        //1.修改对象状态
        self.PromiseState = 'success';
        //2.修改结果值
        self.PromiseResult = data;
        setTimeout(() => {
            //如果callback有这个onReceived属性，就执行
            self.callback.forEach(item => {
                item.onReceived(data);
            });
        });
    }

    function reject(data) {
        if (self.PromiseState !== 'pending') return;
        //1.修改对象状态
        self.PromiseState = 'fail';
        //2.修改结果值
        self.PromiseResult = data;
        setTimeout(() => {
            //如果callback有这个onRejected属性，就执行
            self.callback.forEach(item => {
                item.onRejected(data);
            });
        });
    }

    try {
        //同步调用执行器函数
        executor(resolve, reject);
    } catch (e) {
        reject(e);
    }

}

//添加then方法
Promise.prototype.then = function(onReceived, onRejected) {
        const self = this;
        if (typeof onRejected !== 'function') {
            onRejected = reason => {
                throw reason;
            }
        }
        if (typeof onReceived !== 'function') {
            onReceived = value => value;
        }

        return new Promise((resolve, reject) => {

            function callback(type) {
                //try catch解决then中throw 异常问题
                try {
                    let rs = type(self.PromiseResult);
                    //解决then中返回Promise对象
                    if (rs instanceof Promise) {
                        rs.then(v => {
                            resolve(v);
                        }, r => {
                            reject(r);
                        });
                    } else {
                        //解决then中返回int或非Promise对象
                        resolve(rs);
                    }
                } catch (e) {
                    reject(e);
                }
            }

            if (this.PromiseState === 'success') {
                setTimeout(()=> {
                    callback(onReceived);
                });
            }

            if (this.PromiseState === 'fail') {
                setTimeout(()=> {
                    callback(onRejected);
                });
            }

            if (this.PromiseState === 'pending') {
                this.callback.push({

                    //异步调用处理
                    onReceived: function() {
                        callback(onReceived);
                    },

                    onRejected: function() {
                        callback(onRejected);
                    }
                });
            }
        });

    } // end of Promise.prototype.then

Promise.prototype.catch = function(onRejected) {
        return this.then(undefined, onRejected);
    } // end of Promise.prototype.catch

Promise.resolve = function(value) {
    return new Promise((resolve, reject) => {
        if (value instanceof Promise) {
            value.then(v => {
                resolve(v);
            }, r => {
                reject(r);
            });
        } else {
            //解决then中返回int或非Promise对象
            resolve(value);
        }
    });
}

Promise.reject = function(reason) {
    return new Promise((resolve, reject) => {
        reject(reason);
    });
}

Promise.all = function(promises) {
    return new Promise((resolve, reject) => {
        let count = 0;
        let arr = [];
        for (let i = 0; i < promises.length; i++) {
            promises[i].then(v => {
                    count++;
                    arr[i] = v;
                    if (count == promises.length) {
                        resolve(arr);
                    }
                },
                r => {
                    reject(r);
                });
        }

    });
}

Promise.race = function(promises) {
    return new Promise((resolve, reject) => {
        for (let i = 0; i < promises.length; i++) {
            promises[i].then(v => {
                    resolve(v);
                },
                r => {
                    reject(r);
                });
        }
    });
}