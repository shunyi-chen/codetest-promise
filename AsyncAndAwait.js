const fs = require('fs');
const { promisify } = require("util");
let mineReadFile = promisify(fs.readFile);

// mineReadFile('./resources/File.txt').then(value=>{
//   console.log(value.toString());
// });


//回调函数的方式
// fs.readFile('./resources/File.txt', (err, data1) => {
//     if(err) throw err;
//     fs.readFile('./resources/File.txt', (err, data2) => {
//         if(err) throw err;
//         fs.readFile('./resources/File.txt', (err, data3) => {
//             if(err) throw err;
//             console.log(data1+data2+data3);
//         });

//     });

// });

//async和await
async function main() {
    try {
        let data1 = await mineReadFile('./resources/File.txt');
        let data2 = await mineReadFile('./resources/File.txt');
        let data3 = await mineReadFile('./resources/File.txt');
        console.log(data1+data2+data3);
    } catch(e) {
        console.warn(e);
    }
}

main();